﻿using System;
using System.IO;
using System.Windows.Forms;

namespace PersonDatabaseClient
{
    public partial class MainForm : Form
    {
        private System.CodeDom.Compiler.TempFileCollection tempfile = new System.CodeDom.Compiler.TempFileCollection();
        private PersonDatabase.PersonDatabase pd = new PersonDatabase.PersonDatabase(); // WebReferenceName.LasernetModuleName

        public MainForm()
        {
            InitializeComponent();

            pd.Url = @"http://localhost:8080/webinputport/PersonDatabase/webservice/";
        }

        private void tsbPDFReport_Click(object sender, EventArgs e)
        {
            string tempFileName = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + ".pdf";
            tempfile.AddFile(tempFileName, false);
            try
            {
                File.WriteAllBytes(tempFileName, pd.Report());
                var psi = new System.Diagnostics.ProcessStartInfo(tempFileName);
                psi.UseShellExecute = true;
                System.Diagnostics.Process.Start(psi); // start associated pdf viewer
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Unable to connect to web service ({0})!", ex.Message));
            }
        }

        private void toolStripMenuItem2_CheckedChanged(object sender, EventArgs e)
        {
            // used for making sure only one month and one day is checked at a time
            var me = (sender as ToolStripMenuItem);
            if (me.Checked)
            {
                foreach (ToolStripItem item in me.GetCurrentParent().Items)
                {
                    var castitem = (item as ToolStripMenuItem);
                    if (item != sender && castitem.Checked)
                    {
                        castitem.Checked = false;
                        break; // Only one item can be selected at a time, so there is no need to continue
                    }
                }
            }
        }

        private int GetCheckedValue(ref ToolStripSplitButton button)
        {
            int result = 1;
            foreach (ToolStripItem item in button.DropDownItems)
            {
                if ((item as ToolStripMenuItem).Checked)
                {
                    result = Convert.ToInt32(item.Text);
                    break;
                }
            }
            return result;
        }

        private void tsbPresent_Click(object sender, EventArgs e)
        {
            int month = GetCheckedValue(ref tssbMonth);
            int day = GetCheckedValue(ref tssbDay);
            try
            {
                DateTime datetime = new DateTime(2000, month, day);

                try
                {
                    bindingSource1.DataSource = pd.Birthday(datetime);
                    dataGridView1.DataSource = bindingSource1; // must be set after first read of data to create columns
                }
                catch (Exception ex)
                {
                    MessageBox.Show(String.Format("Unable to connect to web service ({0})!", ex.Message));
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show(String.Format("Invalidate date ({0})!", ex.Message));
            }
        }

        private void tsbAllPersons_Click(object sender, EventArgs e)
        {
            try
            {
                bindingSource1.DataSource = pd.SelectAll();
                dataGridView1.DataSource = bindingSource1; // must be set after first read of data to create columns
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Unable to connect to web service ({0})!", ex.Message));
            }
        }

        private void tsbAddPerson_Click(object sender, EventArgs e)
        {
            var person = new PersonDatabase.Person();
            person.Zipcode = 90210;
            person.City = "Beverly Hills";
            person.Country = "United States of America";
            person.Birthday = System.DateTime.Today;

            var dialog = new PersonData(ref person);
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    int number = pd.Add(person);
                    MessageBox.Show(String.Format("Person was assigned number '{0}'.", number));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(String.Format("Unable to connect to web service ({0})!", ex.Message));
                }
            }
        }

        private void tsbEditPerson_Click(object sender, EventArgs e)
        {
            try
            {
                var number = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells["Number"].Value.ToString());

                var person = pd.Select(number); // get fresh data from database

                var dialog = new PersonData(ref person);
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    if (!pd.Edit(person))
                        MessageBox.Show(String.Format("Unable to edit person ({0}). Might not exist!?", person.Number));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Unable to connect to web service ({0})!", ex.Message));
            }
        }

        private void tsbRemovePerson_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                int number = Convert.ToInt32(row.Cells["Number"].Value.ToString());
                try
                {
                    if (!pd.Remove(number))
                        MessageBox.Show(String.Format("Unable to remove person ({0}). Might not exist!?", number));
                    else
                        dataGridView1.Rows.Remove(row);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(String.Format("Unable to connect to web service ({0})!", ex.Message));
                    break;
                }
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            tsbRemovePerson.Enabled = (dataGridView1.SelectedRows.Count > 0);
            tsbEditPerson.Enabled = (dataGridView1.SelectedRows.Count == 1);
        }
    }
}
