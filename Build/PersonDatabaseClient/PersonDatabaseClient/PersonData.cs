﻿using System.Windows.Forms;

namespace PersonDatabaseClient
{
    public partial class PersonData : Form
    {
        public PersonData(ref PersonDatabase.Person person)
        {
            InitializeComponent();

            tbFirstName.DataBindings.Add(new Binding("Text", person, "FirstName", true, DataSourceUpdateMode.OnPropertyChanged));
            tbLastName.DataBindings.Add(new Binding("Text", person, "LastName", true, DataSourceUpdateMode.OnPropertyChanged));
            tbAddress.DataBindings.Add(new Binding("Text", person, "Address", true, DataSourceUpdateMode.OnPropertyChanged));
            nudZipCode.DataBindings.Add(new Binding("Value", person, "ZipCode", true, DataSourceUpdateMode.OnPropertyChanged));
            tbCity.DataBindings.Add(new Binding("Text", person, "City", true, DataSourceUpdateMode.OnPropertyChanged));
            tbCountry.DataBindings.Add(new Binding("Text", person, "Country", true, DataSourceUpdateMode.OnPropertyChanged));
            tbPhone.DataBindings.Add(new Binding("Text", person, "Phone", true, DataSourceUpdateMode.OnPropertyChanged));
            dtpBirthday.DataBindings.Add(new Binding("Value", person, "Birthday", true, DataSourceUpdateMode.OnPropertyChanged));
        }
    }
}
